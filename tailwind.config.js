module.exports = {
  theme: {
    extend: {
      colors: {
        'regal-blue': '#243c5a',
        'primarydark': '#4d426d',
        'primarylight': '#5c4f82',
        'secondary': '#efa985'
      }
    }
  }
}
